#   Copyright
#
#	Copyright (C) 2009-2010 Jari Aalto
#
#   License
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#   Description
#
#	To learn what TOP LEVEL sections to use in manual pages,
#	see POSIX/Susv standard and "Utility Description Defaults" at
#	http://www.opengroup.org/onlinepubs/009695399/utilities/xcu_chap01.html#tag_01_11
#
#	This is manual page in Perl POD format. Read more at
#	http://perldoc.perl.org/perlpod.html or run command:
#
#	    perldoc perlpod | less
#
#	To check the syntax:
#
#	    podchecker *.pod
#
#	Create manual page with command:
#
#	    pod2man PAGE.N.pod > PAGE.N

=pod

=head1 NAME

posixovl -- FUSE file system that provides POSIX functionality

=head1 SYNOPSIS

    mount.posixovl [-F] [-S SOURCE_DIR] TARGET_DIR [-- fuseopts]

=head1 DESCRIPTION

If no source directory is given, the TARGET_DIR specifies both source
and target (mount point), yielding an "over mount".

Supports: chmod, chown, hardlink, mkfifo, mknod, symlink/readlink
ACLs/xattrs (only in passthrough mode, no emulation).

=head1 NOTES

Using posixovl on an already POSIX-behaving file system (e.g. XFS)
incurs some issues, since detecting whether a path is POSIX behaving
or not is difficult. Hence, the following decision was made:

    - permissions will be set to the default permissions (see below) unless
      a HCB is found that can override these
    - all lower-level files will be operated on/created with the user who
      initiated the mount

If no HCB exists for a file or directory, the default permissions are
644 and 755, respectively. The owner and group of the inode will be the
owner/group of the real file.

Each non-regular, non-directory virtual file will have a zero-size real
file. Simplifies handling, and makes it apparent the object exists
when using other operating system.

Command df(1) will show:

    $ df -Tah
    File System    Type    Size  Used Avail Use% Mounted on
    /dev/hda5     vfat    5.9G  2.1G  3.9G  35% /windows/D
    posix-overlay(/windows/D)
	 fuse.posixovl    5.9G  2.1G  3.9G  35% /windows/D

=head1 OPTIONS

=over 4

=item B<-F>

Option B<-F> will disable permission and ownership checks that would
be required in case you have a POSIX mount over VFAT. For example,
where /vfat is vfat, and /vfat/xfs is a POSIX-behaving file system.

=back

=head1 EXAMPLES

In general, posixovl does not handle case-insensitivity of the
underlying file system (in case of VFAT, for example). If you create a
file I<X0> on VFAT, it is usually lowercased to I<x0>, which may break
some software, namely X.org. In order to make VFAT behave more
POSIX-like, the following mount options are recommended:

    mount -t vfat /dev/sda5 /mnt/vfat -o check=s,shortname=mixed

=head1 ENVIRONMENT

None.

=head1 FILES

None.

=head1 SEE ALSO

mount(1)
umount(1)

=head1 AUTHORS

Program was written by Jan Engelhardt <jengelh@users.sourceforge.net>.

This manual page was written by Jari Aalto <jari.aalto@cante.net>, for
the Debian GNU system (but may be used by others). Released under
license GNU GPL version 2 or (at your option) any later version. For
more information about license, visit
<http://www.gnu.org/copyleft/gpl.html>.

=cut
